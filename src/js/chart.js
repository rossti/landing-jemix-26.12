$( document ).ready(function() {
    if(window.matchMedia('(max-width: 1023px)').matches) {
        $('.guaranteed-sale__chart').circleProgress({
            value: 0.37,
            size: 262,
            startAngle: ((Math.PI) / 2) + Math.PI,
            fill: {
                gradient: ["#f0881f", "#e05c16"]
            },
            duration: 5000
        });
    } else {
        $('.guaranteed-sale__chart').circleProgress({
            value: 0.37,
            size: 330,
            startAngle: ((Math.PI) / 2) + Math.PI,
            fill: {
                gradient: ["#f0881f", "#e05c16"]
            },
            duration: 5000
        });
    }
    var forChartValue;
    var curValue = $('#current-value');
    var timer = setInterval(addTxt, 50);
    function addTxt() {
        if (parseInt(curValue.text()) > 36 ) {
            clearInterval(timer);
            forChartValue = parseInt(curValue.text());
        } else {
            curValue = curValue.text(parseInt(curValue.text()) + 1);
        }
    }
});